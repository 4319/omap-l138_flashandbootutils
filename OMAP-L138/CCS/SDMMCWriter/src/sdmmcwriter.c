/*
 * sdmmcwriter.c
*/

/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/ 
*/
/* 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
/* --------------------------------------------------------------------------
    FILE        : sdmmcwriter.c 				                             	 	        
    PROJECT     : OMAP-L138 CCS SD/MMC Flashing Utility
    AUTHOR      : Daniel Allred
    DESC        : Main function for flashing the SD/MMC device on the OMAP-L138  
 ----------------------------------------------------------------------------- */

// C standard I/O library
#include "stdio.h"

// General type include
#include "tistdtypes.h"

// Device specific CSL
#include "device.h"

// Misc. utility function include
#include "util.h"

// Debug module
#include "debug.h"

// SDMMC memory driver include
#include "sdmmc.h"
#include "sdmmc_mem.h"
#include "device_sdmmc.h"
#include "sdmmcboot.h"

// This module's header file 
#include "sdmmcwriter.h"


/************************************************************
* Explicit External Declarations                            *
************************************************************/


/************************************************************
* Local Macro Declarations                                  *
************************************************************/


/************************************************************
* Local Typedef Declarations                                *
************************************************************/


/************************************************************
* Local Function Declarations                               *
************************************************************/

static Uint32 LOCAL_sdmmcwriter(void);
static Uint32 LOCAL_GetAndWriteFileData(SDMMC_MEM_InfoHandle hSdmmcMemInfo, String fileName, Uint32 destAddr, Bool useHeader);


/************************************************************
* Local Variable Definitions                                *
************************************************************/


/************************************************************
* Global Variable Definitions
************************************************************/

Uint8 *gMemTx, *gMemRx;


/************************************************************
* Global Function Definitions                               *
************************************************************/

void main( void )
{
  Uint32 status;

  // Init memory alloc pointer to start of DDR heap
  UTIL_setCurrMemPtr(0);

  // System init
  if (DEVICE_init() !=E_PASS)
  {
    exit();
  }

  // Execute the SDMMC flashing
  status = LOCAL_sdmmcwriter();

  if (status != E_PASS)
  {
    DEBUG_printString("\tSD/MMC flashing failed!\r\n");
    //Try one more time
 	status = LOCAL_sdmmcwriter();

    if (status != E_PASS)
    {
      DEBUG_printString("\tSD/MMC flashing failed 2nd time!\r\n");
  
    }
    else
    {
      DEBUG_printString("\tSD/MMC boot preparation was successful!\r\n" );
    }
  }
  else
  {
    DEBUG_printString("\tSD/MMC boot preparation was successful!\r\n" );
  }


}


/************************************************************
* Local Function Definitions                                *
************************************************************/

static Uint32 LOCAL_sdmmcwriter()
{
  SDMMC_MEM_InfoHandle hSDMMCMemInfo;

  Int8	fileName[256];
  Uint32  baseAddress = 0;
  Bool  useHeaderForApp = FALSE;

  DEBUG_printString( "Starting " );
  DEBUG_printString( (String) devString );
  DEBUG_printString( " SDMMCWriter.\r\n" );
  
  // Initialize SD/MMC Memory Device
  hSDMMCMemInfo = SDMMC_MEM_open(DEVICE_SDMMCBOOT_PERIPHNUM, hDEVICE_SDMMC_config);
  if (hSDMMCMemInfo == NULL)
  {
    DEBUG_printString( "\tERROR: SDMMC Memory Initialization failed.\r\n" );
    return E_FAIL;
  }
  
  // Allocate temp buffers
  gMemTx = (Uint8 *) UTIL_allocMem(hSDMMCMemInfo->hSDMMCInfo->dataBytesPerBlk);
  gMemRx = (Uint8 *) UTIL_allocMem(hSDMMCMemInfo->hSDMMCInfo->dataBytesPerBlk);

  DEBUG_printString("Will you be writing a UBL image? (Y or y) \r\n");
  DEBUG_readString(fileName);
  fflush(stdin);

  if ((strcmp(fileName,"y") == 0) || (strcmp(fileName,"Y") == 0))
  {
    // Read the AIS file from host
    DEBUG_printString("Enter the binary AIS UBL file name (enter 'none' to skip): \r\n");
    DEBUG_readString(fileName);
    fflush(stdin);
    
    LOCAL_GetAndWriteFileData(hSDMMCMemInfo, fileName, baseAddress, FALSE);
    
    // Assume that the UBL will fit in the first block of the SPI flash
    baseAddress += 0x10000;
    useHeaderForApp = TRUE;
  }

  // Read the AIS file from host
  DEBUG_printString("Enter the application file name (enter 'none' to skip): \r\n");
  DEBUG_readString(fileName);
  fflush(stdin);
  
  if (LOCAL_GetAndWriteFileData(hSDMMCMemInfo, fileName, baseAddress, useHeaderForApp) != E_PASS)
  {
    DEBUG_printString("SDMMC Flashing Failed!\n");
    return E_FAIL;
  }

  return E_PASS;
}

static Uint32 LOCAL_GetAndWriteFileData(SDMMC_MEM_InfoHandle hSdmmcMemInfo, String fileName, Uint32 destAddr, Bool useHeader)
{
  FILE	*fPtr;
  Uint8	*appPtr;
  Int32	fileSize = 0, numBlks;
  int i = 0;

  if (strcmp(fileName,"none") != 0)
  {
    // Open an File from the hard drive
    fPtr = fopen(fileName, "rb");
    if(fPtr == NULL)
    {
      DEBUG_printString("\tERROR: File ");
      DEBUG_printString(fileName);
      DEBUG_printString(" open failed.\r\n");
      return E_FAIL;
    }

    // Initialize the pointer
    fileSize = 0;

    // Read file size
    fseek(fPtr,0,SEEK_END);
    fileSize = ftell(fPtr);

    // Check to make sure image was read correctly
    if(fileSize == 0)
    {
      DEBUG_printString("\tERROR: File read failed.\r\n");
      fclose (fPtr);
      return E_FAIL;
    }
    
    // Find out number of blocks to be written (i.e. round up to nearest block)
    numBlks = (fileSize + hSdmmcMemInfo->hSDMMCInfo->dataBytesPerBlk - 1) >> hSdmmcMemInfo->hSDMMCInfo->dataBytesPerBlkPower2;
  
    // Setup pointer in RAM (rounded-up to multiple of block size)
    appPtr = (Uint8 *) UTIL_allocMem(numBlks * hSdmmcMemInfo->hSDMMCInfo->dataBytesPerBlk);

    fseek(fPtr,0,SEEK_SET);

    if (fileSize != fread(appPtr, 1, fileSize, fPtr))
    {
      DEBUG_printString("\tWARNING: File Size mismatch.\r\n");
    }

    fclose (fPtr);

#if (0)    
    if (SDMMC_MEM_writeBytes(hSdmmcMemInfo, destAddr, fileSize, appPtr) != E_PASS)
    {
      DEBUG_printString("\tERROR: Writing SDMMC failed.\r\n");
      return E_FAIL;
    }
    
    UTIL_waitLoop(100000);
#endif    
    
    // Verify the data written, one block at a time    
    for (i = 0; i< numBlks; i++)
    {
      if (SDMMC_MEM_writeBytes(hSdmmcMemInfo, 
        destAddr, 
        hSdmmcMemInfo->hSDMMCInfo->dataBytesPerBlk,
        &appPtr[i*hSdmmcMemInfo->hSDMMCInfo->dataBytesPerBlk]) != E_PASS)
      {
        DEBUG_printString("\tERROR: Writing SDMMC failed.\r\n");
        return E_FAIL;
      }
      
      UTIL_waitLoop(100000);
        
      if (SDMMC_MEM_verifyBytes(hSdmmcMemInfo, 
        destAddr, 
        hSdmmcMemInfo->hSDMMCInfo->dataBytesPerBlk, 
        &appPtr[i*hSdmmcMemInfo->hSDMMCInfo->dataBytesPerBlk],
        gMemRx) != E_PASS)
      {
        DEBUG_printString("\tERROR: Verifying SDMMC failed.\r\n");
        return E_FAIL;
      }
      destAddr += hSdmmcMemInfo->hSDMMCInfo->dataBytesPerBlk;
    }
  }  
  return E_PASS;
}


/***********************************************************
* End file                                                 *
***********************************************************/
