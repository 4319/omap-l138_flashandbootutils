#############################################################
# Common build definitions for Makefile use                 #
#############################################################
#
ARM_TOOLS_PATH=
ARM_TOOLS_PREFIX?=arm-none-eabi-

CROSSCOMPILE=$(ARM_TOOLS_PREFIX)
ARM_CROSSCOMPILE=$(CROSSCOMPILE)

DSP_TOOLS_PATH?=/home/iakov/ti/c6000_7.4.22/

DSP_LIB_PATH=$(DSP_TOOLS_PATH)lib/
DSP_CROSSCOMPILE=$(DSP_TOOLS_PATH)bin/
