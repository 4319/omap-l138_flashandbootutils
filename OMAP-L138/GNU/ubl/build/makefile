###########################################################################
#                                                                         #
#   Copyright (C) 2012 Texas Instruments Incorporated                     #
#     http://www.ti.com/                                                  #
#                                                                         #
###########################################################################



#############################################################################
#                                                                           #
#  Redistribution and use in source and binary forms, with or without       #
#  modification, are permitted provided that the following conditions       #
#  are met:                                                                 #
#                                                                           #
#    Redistributions of source code must retain the above copyright         #
#    notice, this list of conditions and the following disclaimer.          #
#                                                                           #
#    Redistributions in binary form must reproduce the above copyright      #
#    notice, this list of conditions and the following disclaimer in the    #
#    documentation and/or other materials provided with the                 #
#    distribution.                                                          #
#                                                                           #
#    Neither the name of Texas Instruments Incorporated nor the names of    #
#    its contributors may be used to endorse or promote products derived    #
#    from this software without specific prior written permission.          #
#                                                                           #
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      #
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        #
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    #
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     #
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    #
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT         #
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,    #
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY    #
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      #
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE    #
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.     #
#                                                                           #
#############################################################################
#############################################################
# Makefile for TI UBL project.                              #
#   Generates the binary UBL file can be used as the        #
#   secondary bootloader for the device in NOR, NAND,       #
#   and UART boot modes.                                    #
#############################################################
#

# CROSSCOMPILE definition for entire tree
include ../../../../Common/build.mak
include ../../../device.mak

PROGRAM:=ubl
SOURCES=$(PROGRAM).c device.c uartboot.c device_uart.c uart.c debug.c util.c

CFLAGS_DSP=--abi=coffabi -c -g -DUBL_$(FLASHTYPE) -D$(DEVICETYPE) -I=../../../Common/include -I=../../../../Common/include -I=../../../../Common/arch/c6000/include -I=../../../../Common/$(PROGRAM)/include -I=../../../../Common/ubl/include -I=../../../../Common/drivers/include -I=../../../../Common/gnu/include -I=../../../../Common/ubl/include -DAIS_RBL
CFLAGS_GCC=-DUBL_$(FLASHTYPE) -D$(DEVICETYPE) -I../../../Common/include -I../../../../Common/include -I../../../../Common/arch/c6000/include -I../../../../Common/$(PROGRAM)/include -I../../../../Common/ubl/include -I../../../../Common/drivers/include -I../../../../Common/gnu/include -I../../../../Common/ubl/include -I../../../../Common/arch/arm926ejs/include -DAIS_RBL

LNKFLAGS_DSP=-z -c -estart -a -w -x  --search_path=$(DSP_LIB_PATH)
LNKFLAGS_GCC=-Wl,-T$(LINKERSCRIPT) -nostdlib 
AFLAGS=

ifeq ($(FLASHTYPE),NAND)
  SOURCES+= async_mem.c device_async_mem.c nand.c device_nand.c nandboot.c
endif
ifeq ($(FLASHTYPE),NOR)
  SOURCES+= async_mem.c device_async_mem.c nor.c norboot.c
endif
ifeq ($(FLASHTYPE),SDMMC)
  SOURCES+= sdmmc.c sdmmcboot.c
endif
ifeq ($(FLASHTYPE),ONENAND)
  SOURCES+= async_mem.c device_async_mem.c onenand.c onenandboot.c
endif
ifeq ($(FLASHTYPE),I2C_MEM)
  SOURCES+= i2c.c i2c_mem.c device_i2c.c i2c_memboot.c
endif
ifeq ($(FLASHTYPE),SPI_MEM)
  SOURCES+= spi.c spi_mem.c device_spi.c spi_memboot.c
endif

ifeq ($(DEVICETYPE),OMAPL138)
  CC=$(CROSSCOMPILE)gcc
  SOURCES+= start.asm
  CFLAGS = $(CFLAGS_GCC) -c -Os -Wall -ffreestanding -DUBL_$(FLASHTYPE) -D$(DEVICETYPE) -o $@   
  LNKFLAGS = $(LNKFLAGS_GCC) -o $@ $(LINKERSCRIPT) $(OBJECTS)
  LINKERSCRIPT:=../ARM_$(PROGRAM).lds
  AFLAGS = -x assembler
endif
ifeq ($(DEVICETYPE),AM1808)
  CC=$(CROSSCOMPILE)gcc
  SOURCES+= start.asm
  CFLAGS = $(CFLAGS_GCC) -c -Os -Wall -ffreestanding -DUBL_$(FLASHTYPE) -D$(DEVICETYPE) -o $@   
  LNKFLAGS = $(LNKFLAGS_GCC) -o $@ $(LINKERSCRIPT) $(OBJECTS)
  LINKERSCRIPT:=../ARM_$(PROGRAM).lds
  AFLAGS = -x assembler
endif
ifeq ($(DEVICETYPE),AM1810)
  CC=$(CROSSCOMPILE)gcc
  SOURCES+= start.asm
  CFLAGS = $(CFLAGS_GCC) -c -Os -Wall -ffreestanding -DUBL_$(FLASHTYPE) -D$(DEVICETYPE) -o $@   
  LNKFLAGS = $(LNKFLAGS_GCC) -o $@ $(LINKERSCRIPT) $(OBJECTS)
  LINKERSCRIPT:=../ARM_$(PROGRAM).lds
  AFLAGS = -x assembler
endif
ifeq ($(DEVICETYPE),OMAPL138_LCDK)
  CC=$(CROSSCOMPILE)gcc
  SOURCES+= start.asm
  CFLAGS = $(CFLAGS_GCC) -c -Os -Wall -ffreestanding -DUBL_$(FLASHTYPE) -D$(DEVICETYPE) -o $@   
  LNKFLAGS = $(LNKFLAGS_GCC) -o $@ $(LINKERSCRIPT) $(OBJECTS)
  LINKERSCRIPT:=../ARM_$(PROGRAM).lds
  AFLAGS = -x assembler
endif
ifeq ($(DEVICETYPE),C6748)
  CC=$(DSP_CROSSCOMPILE)cl6x
  SOURCES+= start_c674x.asm pru.c
  CFLAGS = $(CFLAGS_DSP) -ms3 -mo -mv6740 --obj_extension=$(DEVICETYPE)_$(FLASHTYPE).obj
  LNKFLAGS = $(LNKFLAGS_DSP) -o=$@ $(LINKERSCRIPT) $(OBJECTS) --library=libc.a
  LINKERSCRIPT:=../DSP_$(PROGRAM).cmd
endif
ifeq ($(DEVICETYPE),C6748_LCDK)
  CC=$(DSP_CROSSCOMPILE)cl6x
  SOURCES+= start_c674x.asm pru.c
  CFLAGS = $(CFLAGS_DSP) -ms3 -mo -mv6740 --obj_extension=$(DEVICETYPE)_$(FLASHTYPE).obj
  LNKFLAGS = $(LNKFLAGS_DSP) -o=$@ $(LINKERSCRIPT) $(OBJECTS) --library=libc.a
  LINKERSCRIPT:=../DSP_$(PROGRAM).cmd
endif
ifeq ($(DEVICETYPE),C6746)
  CC=$(DSP_CROSSCOMPILE)cl6x
  SOURCES+= start_c674x.asm pru.c
  CFLAGS = $(CFLAGS_DSP) -ms3 -mo -mv6740 --obj_extension=$(DEVICETYPE)_$(FLASHTYPE).obj
  LNKFLAGS = $(LNKFLAGS_DSP) -o=$@ $(LINKERSCRIPT) $(OBJECTS) --library=libc.a
  LINKERSCRIPT:=../DSP_$(PROGRAM).cmd
endif

OBJECTS:=$(patsubst %.c,%.$(DEVICETYPE)_$(FLASHTYPE).obj,$(SOURCES))
OBJECTS:=$(patsubst %.asm,%.$(DEVICETYPE)_$(FLASHTYPE).obj,$(OBJECTS))
EXECUTABLE:=../$(PROGRAM)_$(DEVICETYPE)_$(FLASHTYPE).out
AIS_OUTPUT:=$(patsubst %.out,%.bin,$(EXECUTABLE))

HEX_AIS_PATH:=../../AISUtils/
HEX_AIS_EXECUTABLE:=$(HEX_AIS_PATH)/HexAIS_$(DEVSTRING).exe

ifeq ($(FLASHTYPE),NOR)
  HEX_AIS_INI:=../$(PROGRAM)_nor_hexais.ini
else
  HEX_AIS_INI:=../$(PROGRAM)_hexais.ini
endif

all: $(AIS_OUTPUT) $(LINKERSCRIPT) makefile

.PHONY : clean objclean
clean:
		-rm -f -v $(OBJECTS) $(EXECUTABLE) $(AIS_OUTPUT)

objclean:
		-rm -f -v $(OBJECTS)

$(HEX_AIS_EXECUTABLE):
		make -C $(HEX_AIS_PATH)/HexAIS all
        
$(AIS_OUTPUT):$(EXECUTABLE) $(HEX_AIS_EXECUTABLE)
ifeq ($(shell uname),Linux)
		mono $(HEX_AIS_EXECUTABLE) -ini $(HEX_AIS_INI) -o $@ $< 
else
		$(HEX_AIS_EXECUTABLE) -ini $(HEX_AIS_INI) -o $@ $< 
endif

$(EXECUTABLE): $(OBJECTS)
		$(CC) $(LNKFLAGS)

%.$(DEVICETYPE)_$(FLASHTYPE).obj : %.asm
		$(CC) $(CFLAGS) $(AFLAGS) $<
    
%.$(DEVICETYPE)_$(FLASHTYPE).obj : %.c
		$(CC) $(CFLAGS) $< 

vpath %.h ../../../Common/include:../../../../Common/include:../../../../Common/arch/arm926ejs/include:../../../../Common/$(PROGRAM)/include:../../../../Common/ubl/include:../../../../Common/gnu/include:../../../../Common/drivers/include
vpath %.c ../../../Common/src:../../../../Common/src:../../../../Common/arch/arm926ejs/src:../../../../Common/$(PROGRAM)/src:../../../../Common/ubl/src:../../../../Common/gnu/src:../../../../Common/drivers/src
vpath %.asm ../../../../Common/arch/arm926ejs:../../../../Common/arch/c6000
