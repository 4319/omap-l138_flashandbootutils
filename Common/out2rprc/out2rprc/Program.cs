/*
 * Program.cs
*/

/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/ 
*/

/* 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace out2rprc
{
    class Program
    {
        static void Main(string[] args)
        {
            String input_file_name_1, input_file_name_2, output_file_name;

            // check usage
            if (args.Length == 2)
            {
                // 2 args: 1 input file and 1 output file
                input_file_name_1 = args[0];
                input_file_name_2 = "";
                output_file_name = args[1];
            }
            else if (args.Length == 3)
            {
                // 3 args: 2 input files and 1 output file
                input_file_name_1 = args[0];
                input_file_name_2 = args[1];
                output_file_name = args[2];
            }
            else
            {
                // print usage instructions and quit
                Console.WriteLine("Usage:");
                Console.WriteLine("out2rprc.exe <input file 1 (COFF or ELF)> [<input file 2 (COFF or ELF)] <output file (RPRC)>");
                return;
            }

            // convert file to RPRC binary format
            try
            {
                Out2Rprc.Convert(input_file_name_1, input_file_name_2, output_file_name);
                Console.WriteLine("File conversion complete!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Encountered exception:");
                Console.WriteLine(ex.Message);
            }
        }
    }
}
