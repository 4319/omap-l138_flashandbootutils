/*
 * ConsoleUtilityTest.cs
*/

/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/ 
*/

/* 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
using System;
using System.Text;
using System.IO;
using System.IO.Ports;
using System.Reflection;
using System.Threading;
using System.Globalization;
using TI.UtilLib.ConsoleUtility;

[assembly: AssemblyTitle("ConsoleUtilityTest")]
[assembly: AssemblyVersion("1.00.*")]


namespace TIBootAndFlash
{
  /// <summary>
  /// Main program Class
  /// </summary>
  class Program
  {
    /// <summary>
    /// Main entry point of application
    /// </summary>
    /// <param name="args">Array of command-line arguments</param>
    /// <returns>Return code: 0 for correct exit, -1 for unexpected exit</returns>
    static Int32 Main(String[] args)
    {
      ProgressBar progressBar;
      int blockCnt = 0;
      
      Console.WriteLine("ConsoleUtility Test Program.");
      Console.WriteLine("");
          
      // Test default progress Bar
      progressBar = new ProgressBar();
      progressBar.Update(0.0,"Testing Default ProgressBar");
      blockCnt = 15;
      for (int i = 0; i < blockCnt; i++)
      {
        Thread.Sleep(100);
        progressBar.Percent = (((Double)(i+1))/blockCnt);
        //Console.WriteLine(Console.CursorTop);
      }
      
      progressBar.Update(100.0,"Test Default ProgressBar complete.");
      
      
      // Test progress bar of specified length
      progressBar = new ProgressBar(40);
      progressBar.Update(0.0,"Testing ProgressBar(40)");
      blockCnt = 6;
      for (int i = 0; i < blockCnt; i++)
      {
        Thread.Sleep(100);
        progressBar.Percent = (((Double)(i+1))/blockCnt);
        //Console.WriteLine(Console.CursorTop);
        if (i == 4)
          progressBar.Write(i.ToString() + "\n\n\n" + "\n");
      }
      
      progressBar.Update(100.0,"Test ProgressBar(40) complete.");
      
        
      
      // Test progress bar of specified length and position
      progressBar = new ProgressBar(50,Position.RIGHT,Position.TOP);
      progressBar.Update(0.0,"Testing ProgressBar(50, RIGHT, TOP)");
      blockCnt = 10;
      for (int i = 0; i < blockCnt; i++)
      {
        Thread.Sleep(300);
        progressBar.Percent = (((Double)(i+1))/blockCnt);
        //Console.WriteLine(Console.CursorTop);
      }
      
      progressBar.Update(100.0,"Test ProgressBar(50, RIGHT, TOP) complete.");
      
      
      
      return 0;
    }
  }
}